package com.example.nguyen.util;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.JoinRequest;
import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.Poll;
import com.example.nguyen.model.User;
import com.example.nguyen.payload.ChoiceResponse;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.JoinRequestResponse;
import com.example.nguyen.payload.PollResponse;
import com.example.nguyen.payload.UserSummary;
import com.example.nguyen.repository.ClassRoomRepository;

/**
 * @author nguyen
 *May 5, 2019
 */
public class ModelMapper {

	public static PollResponse mapPollToPollResponse(Poll poll, Map<Long, Long> choiceVotesMap, User creator, Long userVote) {
		PollResponse pollResponse = new PollResponse();
		pollResponse.setId(poll.getId());
		pollResponse.setQuestion(poll.getQuestion());
		pollResponse.setCreationDateTime(poll.getCreatedAt());
		pollResponse.setExpirationDateTime(poll.getExpirationDateTime());
		
		Instant now = Instant.now();
		pollResponse.setIsExpired(poll.getExpirationDateTime().isBefore(now));
		
		List<ChoiceResponse> choiceResponses = poll.getChoices().stream().map(choice -> {
			ChoiceResponse choiceResponse = new ChoiceResponse();
			choiceResponse.setId(choice.getId());
			choiceResponse.setText(choice.getText());
			
			if(choiceVotesMap.containsKey(choice.getId())) {
				choiceResponse.setVoteCount(choiceVotesMap.get(choice.getId()));
			} else {
				choiceResponse.setVoteCount(0);
			}
			return choiceResponse;
		}).collect(Collectors.toList());
		
		pollResponse.setChoices(choiceResponses);
		UserSummary creatorSummary = new  UserSummary(creator.getId(), creator.getUsername(), creator.getName());
		
		pollResponse.setCreatedBy(creatorSummary);
		
		if(userVote != null) {
			pollResponse.setSelectedChoice(userVote);
		}
		
		long totalVotes = pollResponse.getChoices().stream().mapToLong(ChoiceResponse:: getVoteCount).sum();
		pollResponse.setTotalVotes(totalVotes);
		
		return pollResponse;
	};
	
	public static ClassResponse mapClassToClassResponse(ClassRoom classRoom, User creator) {
		ClassResponse classResponse = new ClassResponse();
		classResponse.setId(classRoom.getId());
		classResponse.setClassRoom(classRoom);
		
		classResponse.setCreatedBy(creator);
		
		return classResponse;
		
	}

	public static ClassResponse mapClassToClassResponse(ClassRoom classRoom, User creator, Boolean isSendRequest) {
		System.out.println("calsssss "+ classRoom +isSendRequest);
		ClassResponse classResponse = new ClassResponse();
		classResponse.setId(classRoom.getId());
		classResponse.setClassRoom(classRoom);
		
		classResponse.setCreatedBy(creator);
		classResponse.setSendRequestToJoin(isSendRequest);

		return classResponse;
	}

	public static ClassResponse mapClassToClassResponse(ClassRoom classRoom, User creator, List<JoinRequestResponse> listRequestToJoin) {
		
		ClassResponse classResponse = new ClassResponse();
		classResponse.setId(classRoom.getId());
		classResponse.setClassRoom(classRoom);
		
		classResponse.setCreatedBy(creator);
//		if(listRequestToJoin.size()>0)
			classResponse.setListRequestToJoin(listRequestToJoin);
			System.out.println("calss re "+ classRoom );
		return classResponse;
	}
}
