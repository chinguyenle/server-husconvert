package com.example.nguyen.util;

/**
 * @author nguyen
 *May 5, 2019
 */
public interface AppConstants {

	String DEFAULT_PAGE_NUMBER = "0";
	String DEFAULT_PAGE_SIZE = "30";
	
	int MAX_PAGE_SIZE = 50;
}
