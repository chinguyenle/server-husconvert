package com.example.nguyen.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author nguyen
 *May 14, 2019
 */
@Service
public class ConvertMultiPathToFile {

	public File Convert(MultipartFile multipartFile) throws IOException {
		File convertFile = new File(multipartFile.getOriginalFilename());
		convertFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convertFile);
		fos.write(multipartFile.getBytes());
		fos.close();
		return convertFile;
	}
}
