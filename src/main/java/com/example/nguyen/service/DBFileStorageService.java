package com.example.nguyen.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.nguyen.exception.FileStorageException;
import com.example.nguyen.exception.MyFileNotFoundException;
import com.example.nguyen.model.DBFile;
import com.example.nguyen.repository.DBFileRepository;

/**
 * @author nguyen
 *May 10, 2019
 */
@Service
public class DBFileStorageService {

	@Autowired
	private DBFileRepository dbFileRepository;
	
	public DBFile storeFile(MultipartFile file) {
		
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		System.out.printf("file name, args---" + fileName);
		try {
			if(fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence "+ fileName);
			}
			
			DBFile dbFile = new DBFile(fileName, file.getContentType(), file.getBytes());
			
			return dbFileRepository.save(dbFile);
			
		} catch (IOException e) {
			// TODO: handle exception
			throw new FileStorageException("Could not store file "+fileName+ ".Please try again", e);
		}
		
	}
	
	public DBFile getFile(String fileId) {
		return dbFileRepository.findById(fileId).orElseThrow(
				() -> new MyFileNotFoundException("File not found with id "+ fileId));
	}
	
}
