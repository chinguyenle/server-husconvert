package com.example.nguyen.service;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFNotes;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.jcodec.api.awt.AWTSequenceEncoder;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.nguyen.exception.AppException;
import com.example.nguyen.exception.FileStorageException;
import com.example.nguyen.model.DBFile;
import com.example.nguyen.model.Note;
import com.example.nguyen.model.Slide;
import com.example.nguyen.repository.DBFileRepository;

import net.bytebuddy.implementation.bytecode.Throw;

/**
 * @author nguyen May 14, 2019
 */
@Service
public class CreateImageAndVideo {
	private String private_key= "00fe3017-c945-4c63-96f4-e0d1763c7b93"; // Ngoc="4a3cbad1-399a-477d-9bb0-ec4acc10ae33"
	private static String APPID = "5cb8429999e8dc1a0e077ac4"; //Ngoc- "5cf93706e99bbe27e6a5f086";
	private String voiceDefault = "hn_male_xuantin_vdts_48k-hsmm";
	private String userId = "47673"; // Ngoc-"48330"
	private String KEY = CreateImageAndVideo.encodeMd5(private_key, userId); //"503e98ebe19b445290179243c51bc28c";
	private String serviceType = "1";
	

    @Autowired
    private DBFileStorageService dBFileStorageService;

	@Autowired
	private DBFileRepository dbFileRepository;
	

	public void createVideo(List<BufferedImage> listImage) throws IOException {
		AWTSequenceEncoder enc = AWTSequenceEncoder.create25Fps(new File("video.mp4"));
		int framesForAnImage = 100;
		System.out.println("size" + listImage.size());
		for (int i = 0; i < listImage.size(); i++) {
			for (int j = 0; j < framesForAnImage; j++) {
				enc.encodeImage(listImage.get(i));
			}
		}
		enc.finish();
	}

	public List<Slide> createListSlides(File file, String fileName) throws IOException {
		List<Slide> listSlides = new ArrayList<Slide>();
		FileInputStream fis = new FileInputStream(file);

		XMLSlideShow ppt = new XMLSlideShow(fis);
		fis.close();

		Dimension pgsize = ppt.getPageSize();
		ArrayList<BufferedImage> listImage = new ArrayList<>();
		int idx = 1;
		for (XSLFSlide slide : ppt.getSlides()) {

			BufferedImage img = new BufferedImage(pgsize.width, pgsize.height, BufferedImage.TYPE_INT_RGB);
			Graphics2D graphics = img.createGraphics();
			// clear the drawing area
			graphics.setPaint(Color.white);
			graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));

			// render
			slide.draw(graphics);
			// save the output
//			FileOutputStream out = new FileOutputStream("./newimage/slide-" + idx + ".jpg");
//			ImageIO.write(img, "jpg", out);
//			out.close();
			listImage.add(img);
			String notes = "";

			int indexNote = 0;
			try {
				XSLFNotes mynotes = slide.getNotes();
				for (XSLFShape shape : mynotes) {
					if (shape instanceof XSLFTextShape) {
						XSLFTextShape txShape = (XSLFTextShape) shape;
						for (XSLFTextParagraph xslfParagraph : txShape.getTextParagraphs()) {
//	                        System.out.println(xslfParagraph.getText());
//	                        System.out.println("========indexNote========="+indexNote);
							String note = xslfParagraph.getText().trim();
							if(indexNote == txShape.getTextParagraphs().size()-1 ) {
								int lastIndexOfIdx = note.lastIndexOf(""+idx);
								if(lastIndexOfIdx != -1)
									note = note.substring(0, lastIndexOfIdx);
								note = note.replace("\n", "").replace("\r", "");
							}
							notes = notes + note ;
						}
					}
				}

				indexNote++;
			} catch (Exception e) {
				System.out.println("error ---" + e.getMessage());

			}
//			System.out.println("=================" + notes);
			int startTime = 0;
			if(listSlides.size()>0) {
				startTime = listSlides.get(listSlides.size()-1).getEndTime() ;
			}
			Slide slide2 = createASlide(img, notes, fileName+ "-slide-" + idx, idx, startTime);
			if (slide2 != null) {
				listSlides.add(slide2);
			}

			idx++;
		}
//		System.out.println("=================" + listSlides.size());
		ppt.close();
		return listSlides;
	}

	// create a slide in powerpoint file
	public Slide createASlide(BufferedImage bfImage, String notes, String slideName, int index,int startTime) {
		Slide slide = new Slide();
		DBFile dbFile = saveImageToMySql(bfImage, slideName);
		List<Note> listNotes = createNotes(notes, startTime);
		slide.setSrcSlide(dbFile.getId());
		slide.setNotes(listNotes);
		slide.setStartTime(startTime);
		slide.setEndTime(listNotes.get(listNotes.size()-1).getEndTime());
		return slide;
	}

	// save image to mySql
	public DBFile saveImageToMySql(BufferedImage bfImage, String name) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			ImageIO.write(bfImage, "jpg", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();

			DBFile dbFile = dbFileRepository.save(new DBFile(name, "image/jpeg", imageInByte));
			return dbFile;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("deprecation")
	public List<Note> createNotes(String notes, int startTime) {
		List<Note> listNotes = new ArrayList<Note>();
		StringTokenizer stringTokenizer = new StringTokenizer(notes," ");
		if(stringTokenizer.countTokens() > 20) {
			
			int startIndexNote = 0;
			int startTimeNote = startTime;
			for (int i = 0; i < notes.length(); i++) {
				if(Character.isSpace(notes.charAt(i)) || i==notes.length()-1) {
					String subNoteContent = notes.substring(startIndexNote, i);
//					System.out.print("sub note --- "+subNoteContent );
					StringTokenizer countWords = new StringTokenizer(subNoteContent," ");
					System.out.print("sub note --- "+subNoteContent );
					System.out.println("lentght "+countWords.countTokens());
					if(countWords.countTokens() >= 30 || i==notes.length()-1) {
//						listNoteContent.add(subNote);
						int endTimeNote = startTimeNote + timeToSpeech(countWords.countTokens());
						
						String srcAudioNote ="";// getSrcAudio(notes, "1.0", voiceDefault);

						System.out.println();
						System.out.print("sub note --- "+subNoteContent+"--"+stringTokenizer.countTokens()+" time"+timeToSpeech(stringTokenizer.countTokens()) );
						Note subNote = new Note(subNoteContent, voiceDefault, "1.0", srcAudioNote, startTimeNote, endTimeNote);
						listNotes.add(subNote);
						startTimeNote = endTimeNote;
						startIndexNote = i+1;
					}
				}
				
			}
		}else {
			String srcAudio = "";
			if(notes.length() > 0) {
				srcAudio ="";// getSrcAudio(notes, "1.0", voiceDefault);
			}
//			System.out.println("stringTokenizer----- "+ stringTokenizer.countTokens());
			
			int endTime =startTime + timeToSpeech(stringTokenizer.countTokens());
			Note note = new Note(notes, voiceDefault, "1.0", srcAudio, startTime, endTime);
			listNotes.add(note);
		}
		

		
		return listNotes;

	}

	public int timeToSpeech(int numberOfWords) {
		return (int) Math.ceil(((float)numberOfWords) / 250 * 60);
	}

	public String getSrcAudio(String text, String readingSpeed, String voiceType) {
		if(text.trim().length() < 1) return "";
		String ttsHost = "https://tts.vbeecore.com/api/tts";
		Timestamp ts = new Timestamp( (new Date()).getTime());
		String inputURLWithQuery = null;
		URL ttsRequest = null;
		String urlMp3 = "";
		try {
			inputURLWithQuery = ttsHost
					+"?app_id="+APPID
					+"&key="+KEY
					+"&voice="+voiceType
					+"&rate=1"
					+"&time="+ts.getTime()
					+"&user_id="+userId
					+"&service_type=1"
					+"&input_text="+URLEncoder.encode(text, "UTF-8").replace("+", "%20")
					+"&type_output=link"
					+"&audio_type=mp3";
			
			//Connect server through Http request
			ttsRequest = new URL(inputURLWithQuery);
			System.out.println("url    " + inputURLWithQuery);
			
			HttpURLConnection con = (HttpURLConnection) ttsRequest.openConnection();
			con.setRequestMethod("GET");
			//optional defaulit is get
		     int responseCode = con.getResponseCode();
		     System.out.println("Response Code : " + responseCode);
		     BufferedReader in = new BufferedReader(
		             new InputStreamReader(con.getInputStream()));
		     String inputLine;
		     StringBuffer response = new StringBuffer();
		     while ((inputLine = in.readLine()) != null) {
		      	response.append(inputLine);
		     }
		     in.close();

			// Read JSON response and print
			JSONObject myResponse = new JSONObject(response.toString());
			System.out.println("result after Reading JSON Response");
			System.out.println("link- " + myResponse.getString("link"));
			urlMp3 = myResponse.getString("link");
		} catch (Exception e) {
			throw new AppException("Can't not get url from vbee   ", e);
			
		}
		return urlMp3;
	}
	
	public static String encodeMd5(String privateKey, String userId) {
		Date date= new Date();
		 
		 long time = date.getTime();
		     System.out.println("Time in Milliseconds: " + time);
		String input=privateKey+":"+userId+":"+time;
		System.out.print("inut -- "+input);
		
		 try { 
			  
	            // Static getInstance method is called with hashing MD5 
	            MessageDigest md = MessageDigest.getInstance("MD5"); 
	  
	            // digest() method is called to calculate message digest 
	            //  of an input digest() return array of byte 
	            byte[] messageDigest = md.digest(input.getBytes()); 
	  
	            // Convert byte array into signum representation 
	            BigInteger no = new BigInteger(1, messageDigest); 
	  
	            // Convert message digest into hex value 
	            String hashtext = no.toString(16); 
	            while (hashtext.length() < 32) { 
	                hashtext = "0" + hashtext; 
	            } 

	    		System.out.print("md5  -- "+hashtext);
	            return hashtext; 
	        }  
	  
	        // For specifying wrong message digest algorithms 
	        catch (NoSuchAlgorithmException e) { 
	            throw new RuntimeException(e); 
	        } 
		
	}

}
