package com.example.nguyen.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.nguyen.exception.BadRequestException;
import com.example.nguyen.exception.ResourceNotFoundException;
import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.DBFile;
import com.example.nguyen.model.JoinRequest;
import com.example.nguyen.model.JoinRequestStatus;
import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.RoleName;
import com.example.nguyen.model.User;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.ClassRoomRequest;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.payload.JoinRequestResponse;
import com.example.nguyen.repository.ClassRoomRepository;
import com.example.nguyen.repository.CommentRepository;
import com.example.nguyen.repository.JoinRequestRepository;
import com.example.nguyen.repository.LessonRepository;
import com.example.nguyen.repository.NoteRepository;
import com.example.nguyen.repository.UserRepository;
import com.example.nguyen.security.CurrentUser;
import com.example.nguyen.security.UserPrincipal;
import com.example.nguyen.util.AppConstants;
import com.example.nguyen.util.ModelMapper;

import antlr.debug.NewLineEvent;

/**
 * @author nguyen May 7, 2019
 */

@Service
public class ClassService {

	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private NoteRepository noteRepository;

	@Autowired
	private LessonRepository lessonRepository;

	@Autowired
	private ClassRoomRepository classRoomRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private JoinRequestRepository joinRequestRepository;

	@Autowired
	private DBFileStorageService dBFileStorageService;

	private static final Logger logger = LoggerFactory.getLogger(PollService.class);

//	public PagedResponse<ClassResponse> getAllClasses(UserPrincipal currentUser, int page, int size) {
//
//		validatePageNumberAndSize(page, size);
//		// Retrieve ClassRoom
//		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
//		Page<ClassRoom> classes = classRoomRepository.findAll(pageable);
//
//		if (classes.getNumberOfElements() == 0) {
//			return new PagedResponse<>(Collections.emptyList(), classes.getNumber(), classes.getSize(),
//					classes.getTotalElements(), classes.getTotalPages(), classes.isLast());
//		}
//
//		Map<Long, User> creatorMap = getPollCreatorMap(classes.getContent());
//		
//
//		List<ClassResponse> classResponses = classes.map(classRoom -> {
//			Boolean isSendRequest = false;
////				int count = joinRequestRepository.countRequest(classRoom.getId(), currentUser.getId());
////				System.out.println(" count "+count+" classid"+currentUser.getId());
//				
////				if(count != 0) isSendRequest = true;
//				
//			return ModelMapper.mapClassToClassResponse(classRoom, creatorMap.get(classRoom.getCreatedBy()), isSendRequest);
//		}).getContent();
//
//		return new PagedResponse<>(classResponses, classes.getNumber(), classes.getSize(), classes.getTotalElements(),
//				classes.getTotalPages(), classes.isLast());
//	}

	public ClassResponse createClassRoom(String className, MultipartFile avatar, UserPrincipal currentUser) {

		ClassRoom classRoom = new ClassRoom();

		classRoom.setClassRoomName(className);

		DBFile dbFile = null;
		if (avatar != null) {
			dbFile = dBFileStorageService.storeFile(avatar);
			classRoom.setAvatarId(dbFile.getId());
		}

		User creator = userRepository.getOne(currentUser.getId());

		classRoom.setCreatedBy(currentUser.getId());

		return ModelMapper.mapClassToClassResponse(classRoomRepository.save(classRoom), creator);
	}

	public ClassResponse getClassById(long classId, UserPrincipal currentUser) {

		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));

		User creator = userRepository.findById(classRoom.getCreatedBy())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", classRoom.getCreatedBy()));

		return ModelMapper.mapClassToClassResponse(classRoom, creator);

	}

	public PagedResponse<ClassResponse> getMyClassRoombyUser(long currentUserId, int page, int size) {

		validatePageNumberAndSize(page, size);

		// Retrieve Polls
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
		User user = userRepository.findById(currentUserId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUserId));
		System.out.print("id us----------- " + user.getName() + "-----");
		List<ClassRoom> classes = null;
		if (user.getRoles().iterator().next().getName() == RoleName.ROLE_TEACHER) {
			System.out.println("Teacher");
			classes = classRoomRepository.findByCreatedBy(user.getId(), pageable);
		} else if (user.getRoles().iterator().next().getName() == RoleName.ROLE_USER) {
			System.out.println("User");
			classes = classRoomRepository.findAllClassRoomHasUser(user.getId());
		} else if (user.getRoles().iterator().next().getName() == RoleName.ROLE_ADMIN) {

			classes = new ArrayList<ClassRoom>();
		} else
			classes = new ArrayList<ClassRoom>();
		System.out.println("id us----------- " + classes.size() + "-----");

		Map<Long, User> creatorMap = getPollCreatorMap(classes);

		List<ClassResponse> classResponses = classes.stream().map(classRoom -> {
			List<JoinRequestResponse> listJoinRequestResponse = new ArrayList<JoinRequestResponse>();
			List<JoinRequest> listRequest = joinRequestRepository.getListRequest(classRoom.getId());
			System.out.println("list request " + listRequest.size());
			if (listRequest.size() > 0) {
				listRequest.forEach(request -> {

					listJoinRequestResponse
							.add(new JoinRequestResponse(request.getId(), classRoom.getId(), request.getUser()));
				});
			}
			return ModelMapper.mapClassToClassResponse(classRoom, creatorMap.get(classRoom.getCreatedBy()),
					listJoinRequestResponse);
		}).collect(Collectors.toList());
		System.out.println("size" + classResponses.size());

		return new PagedResponse<>(classResponses, 1, classes.size(), classes.size(), 1, true);

	}

	public ClassResponse addUserToClass(long classId, List<User> listUsers, UserPrincipal currentUser) {

		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));
		User creator = userRepository.findById(classRoom.getCreatedBy())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", classRoom.getCreatedBy()));
		if (listUsers.size() > 0) {
			Set<User> users = classRoom.getUsers();
			listUsers.forEach(user -> users.add(user));

			classRoom.setUsers(users);

			classRoomRepository.save(classRoom);
		}
		return ModelMapper.mapClassToClassResponse(classRoom, creator);
	}

	public ClassResponse deleteUsers(Long classId, List<User> listUsers, UserPrincipal currentUser) {
		// TODO Auto-generated method stub
		List<Long> deletedUserIds = new ArrayList<Long>();
		if (listUsers.size() > 0) {
			listUsers.forEach(user -> deletedUserIds.add(user.getId()));

//    		classRoom.setUsers(users);

			classRoomRepository.deleteUserInClass(classId, deletedUserIds);
		}
		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));

		User creator = userRepository.findById(classRoom.getCreatedBy())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", classRoom.getCreatedBy()));
		return ModelMapper.mapClassToClassResponse(classRoom, creator);
	}

	public PagedResponse<User> getUsersNotInClass(Long classId) {
		// TODO Auto-generated method stub
//        Pageable pageable = PageRequest.of(1, 1, Sort.Direction.DESC, "email");
		List<User> listUser = userRepository.getAllUserNotInClass(classId);
		return new PagedResponse<>(listUser, 1, listUser.size(), listUser.size(), 1, true);
	}

	private void validatePageNumberAndSize(int page, int size) {
		if (page < 0) {
			throw new BadRequestException("Page number cannot be less than zero.");
		}

		if (size > AppConstants.MAX_PAGE_SIZE) {
			throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
		}
	}

	private Map<Long, User> getPollCreatorMap(List<ClassRoom> classes) {
		// Get Poll Creator details of the given list of polls
		List<Long> creatorIds = classes.stream().map(ClassRoom::getCreatedBy).distinct().collect(Collectors.toList());
		List<User> creators = userRepository.findByIdIn(creatorIds);

		Map<Long, User> creatorMap = creators.stream().collect(Collectors.toMap(User::getId, Function.identity()));

		return creatorMap;
	}

	public PagedResponse<ClassResponse> getAllClasses(long currentUserId, int page, int size) {

		validatePageNumberAndSize(page, size);

		// Retrieve Polls
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
		User user = userRepository.findById(currentUserId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUserId));
		List<ClassRoom> classes = classRoomRepository.findAll();
		Map<Long, User> creatorMap = getPollCreatorMap(classes);

		List<ClassResponse> classResponses = classes.stream().map(classRoom -> {
			Boolean isSendRequest = false;
			int count = joinRequestRepository.countRequest(classRoom.getId(), currentUserId);
			System.out.println(" count " + count + " classid" + currentUserId);

			if (count != 0)
				isSendRequest = true;

			return ModelMapper.mapClassToClassResponse(classRoom, creatorMap.get(classRoom.getCreatedBy()),
					isSendRequest);
		}).collect(Collectors.toList());

		return new PagedResponse<>(classResponses, 1, classes.size(), classes.size(), 1, true);
	}

	public void addLessonToClass(Long classId, Long lessonId, UserPrincipal currentUser) {
		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));
		Lesson lesson = lessonRepository.findById(lessonId)
				.orElseThrow(() -> new ResourceNotFoundException("Lesson", "id", lessonId));

		Set<Lesson> lessons = classRoom.getLessons();
		lessons.add(lesson);
		classRoom.setLessons(lessons);
		classRoomRepository.save(classRoom);

	}

	public void deleteLessonToClass(Long classId, Long lessonId, UserPrincipal currentUser) {
		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));
		Lesson lesson = lessonRepository.findById(lessonId)
				.orElseThrow(() -> new ResourceNotFoundException("Lesson", "id", lessonId));

		Set<Lesson> lessons = classRoom.getLessons();
		lessons.remove(lesson);
		classRoom.setLessons(lessons);
		classRoomRepository.save(classRoom);

	}

	public void sendJoinRequest(Long classId, UserPrincipal currentUser) {
		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));
		User user = userRepository.findById(currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUser.getId()));

		JoinRequest joinRequest = new JoinRequest(classRoom, user, JoinRequestStatus.PENDING);

		joinRequestRepository.save(joinRequest);
	}

	public void acceptJoinRequest(Long requestId, Long classId, Long userId) {
		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
		Set<User> users = classRoom.getUsers();
		users.add(user);

		classRoom.setUsers(users);

		classRoomRepository.save(classRoom);
		joinRequestRepository.deleteById(requestId);

	}

	public void cancelJoinRequest(Long requestId) {
		// TODO Auto-generated method stub

		joinRequestRepository.deleteById(requestId);
	}

	public void deleteClass(Long classId) {
		// TODO Auto-generated method stub
		joinRequestRepository.deleteRequestClassId(classId);
		classRoomRepository.deleteClassUser(classId);
		classRoomRepository.deleteClassLesson(classId);
		classRoomRepository.deleteById(classId);
	}

	public void editClass(Long classId, String classRoomName) {
		ClassRoom classRoom = classRoomRepository.findById(classId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", classId));
		classRoom.setClassRoomName(classRoomName);

		classRoomRepository.save(classRoom);

	}

}
