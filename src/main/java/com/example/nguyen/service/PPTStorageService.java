package com.example.nguyen.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.nguyen.exception.FileStorageException;
import com.example.nguyen.exception.MyFileNotFoundException;
import com.example.nguyen.property.FileStorageProperties;

/**
 * @author nguyen May 14, 2019
 */
@Service
public class PPTStorageService {

	private final Path pptStorageLocation;

	public PPTStorageService(FileStorageProperties fileStorageProperties) {
		this.pptStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.pptStorageLocation);
		} catch (Exception e) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored", e);
		}
	}

	public String storeFile(MultipartFile file) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence" + fileName);
			}
			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.pptStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			return fileName;
		} catch (IOException e) {
			throw new FileStorageException("Could not store file" + fileName + ".Please try again!", e);
		}
	}

	public Resource loadFileAsResource(String fileName) {

		try {
			Path filePath = this.pptStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if(resource.exists()) {
				return resource;
			} else {
				throw new MyFileNotFoundException("File not found" + fileName);
			}
		} catch (MalformedURLException e) {
			throw new MyFileNotFoundException("File not found"+ fileName, e);
		}

	}

}
