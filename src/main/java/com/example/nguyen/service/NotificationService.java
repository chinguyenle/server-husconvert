package com.example.nguyen.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.nguyen.exception.ResourceNotFoundException;
import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.Notification;
import com.example.nguyen.model.User;
import com.example.nguyen.payload.LessonResponse;
import com.example.nguyen.payload.NotificationResponse;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.repository.ClassRoomRepository;
import com.example.nguyen.repository.NotificationRepository;
import com.example.nguyen.repository.UserRepository;

/**
 * @author nguyen
 *May 21, 2019
 */

@Service
public class NotificationService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ClassRoomRepository classRoomREpository;
    
	@Autowired
	private NotificationRepository notificationRepository;

	public void createNotification(Long userId, Long classId, String text) {
		Notification notification = new Notification();
		User user = userRepository.findById(userId).orElseThrow(
				() -> new ResourceNotFoundException("User", "id", userId));
		ClassRoom classRoom = classRoomREpository.findById(classId).orElseThrow(
				() -> new ResourceNotFoundException("ClassRoom", "id", classId));
		
		notification.setClassRoom(classRoom);
		notification.setUser(user);
		notification.setText(text);
		notificationRepository.save(notification);
	}
	
	public PagedResponse<NotificationResponse> getNotificationOfClass(Long classId, int page, int size) {
		
//		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");

		List<Notification> notifications = notificationRepository.findByClassRoomId(classId);
		
		List<NotificationResponse> notificationResponses = notifications.stream().map(noti -> {

			User user = userRepository.findById(noti.getCreatedBy()).orElseThrow(
					() -> new ResourceNotFoundException("User", "id", noti.getCreatedBy()));
			NotificationResponse notificationResponse = new NotificationResponse();
			notificationResponse.setClassId(classId);
			notificationResponse.setCreatedAt(noti.getCreatedAt());
			notificationResponse.setCreatedBy(user.getName());
			notificationResponse.setId(noti.getId());
			notificationResponse.setText(noti.getText());
			
			return notificationResponse;
		}).collect(Collectors.toList());
		

		return new PagedResponse<>(notificationResponses,  1, notifications.size(), notifications.size(), 1, true);
	}
}
