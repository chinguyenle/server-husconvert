package com.example.nguyen.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.nguyen.exception.ResourceNotFoundException;
import com.example.nguyen.model.User;
import com.example.nguyen.model.RoleName;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.payload.UserProfile;
import com.example.nguyen.repository.ClassRoomRepository;
import com.example.nguyen.repository.CommentRepository;
import com.example.nguyen.repository.JoinRequestRepository;
import com.example.nguyen.repository.LessonRepository;
import com.example.nguyen.repository.UserRepository;

/**
 * @author nguyen
 *May 12, 2019
 */
@Service
public class UserService {
	

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClassRoomRepository classRoomRepository;
    
    @Autowired
    private CommentRepository commentRepository;
    
    @Autowired
    private JoinRequestRepository joinRequestRepository;
    
    @Autowired
    private LessonRepository lessonRepository;
    
    @Autowired
    private ClassService classService;
    
	public UserProfile getUserProfile(Long userId) {
		
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", userId));;
		
    	List<ClassResponse> joinedClasses = classService.getMyClassRoombyUser(user.getId(), 1, 1).getContent();

    	return new UserProfile(user.getId(), user, joinedClasses);
		
	}

	public PagedResponse<User> getAllUser() {
		// TODO Auto-generated method stub
		List<User> allUsers = userRepository.findAll();
		List<User> users = new ArrayList<User>();
		allUsers.forEach(user -> {
			if(user.getRoles().iterator().next().getName() !=  RoleName.ROLE_ADMIN)
				users.add(user);
		});
		return new PagedResponse<>(users,  1, users.size(), users.size(), 1, true);
	}

	public void deleteUser(Long userId) {
		classRoomRepository.deleteUserInAllClass(userId);
		commentRepository.deleteByUserId(userId);
		joinRequestRepository.deleteByUserId(userId);
		lessonRepository.deleteLikesByUserId(userId);
		userRepository.deleteById(userId);
		
	}
}
