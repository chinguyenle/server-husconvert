package com.example.nguyen.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.nguyen.exception.ResourceNotFoundException;
import com.example.nguyen.model.Comment;
import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.Note;
import com.example.nguyen.model.Slide;
import com.example.nguyen.model.User;
import com.example.nguyen.payload.LessonDetailResponse;
import com.example.nguyen.payload.LessonRequest;
import com.example.nguyen.payload.LessonResponse;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.repository.ClassRoomRepository;
import com.example.nguyen.repository.CommentRepository;
import com.example.nguyen.repository.LessonRepository;
import com.example.nguyen.repository.NoteRepository;
import com.example.nguyen.repository.NotificationRepository;
import com.example.nguyen.repository.SlideRepository;
import com.example.nguyen.repository.UserRepository;
import com.example.nguyen.security.UserPrincipal;

/**
 * @author nguyen May 8, 2019
 */
@Service
public class LessonService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private LessonRepository lessonRepository;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private SlideRepository slideRepository;

	@Autowired
	private ClassRoomRepository classRoomRepository;

	@Autowired
	private NoteRepository noteRepository;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private ConvertMultiPathToFile convert;

	@Autowired
	private CreateImageAndVideo createImageAndVideo;

	private static final Logger logger = LoggerFactory.getLogger(PollService.class);

	public Lesson initLesson(UserPrincipal currentUser, MultipartFile file) {

		String allName = file.getOriginalFilename();
		String name = "";
		int indexLastDot = allName.lastIndexOf(".");
		System.out.println(" file ppt  -----" + indexLastDot);
		if (indexLastDot != -1) {
			name = allName.substring(0, indexLastDot);
		}
		Lesson lesson = new Lesson(name);
		try {
			File convFile = convert.Convert(file);
			List<Slide> listSlides = createImageAndVideo.createListSlides(convFile, file.getOriginalFilename());
			lesson.setSlides(listSlides);
			lesson.setCreatedBy(currentUser.getId());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesson;
	}

	public Lesson createLesson(LessonRequest lessonRequest, Boolean preview) {

		Lesson lesson = new Lesson(lessonRequest.getName());
		List<Slide> listSlides = new ArrayList<Slide>();

		lesson.setAvatarId(lessonRequest.getAvatarId());

		lessonRequest.getSlides().forEach(slideRequest -> {
			List<Note> listNotes = new ArrayList<Note>();
			Slide slide = new Slide();
			slideRequest.getNotes().forEach(noteRequest -> {
				String srcAudio = createImageAndVideo.getSrcAudio(noteRequest.getText(), noteRequest.getReaddingSpeed(),
						noteRequest.getVoiceType());
				slide.addNote(
						new Note(noteRequest.getText(), noteRequest.getVoiceType(), noteRequest.getReaddingSpeed(),
								srcAudio, noteRequest.getStartTime(), noteRequest.getEndTime()));
			});
			slide.setSrcSlide(slideRequest.getSrcSlide());
			slide.setEndTime(slideRequest.getEndTime());
			slide.setStartTime(slideRequest.getStartTime());
			lesson.addSlide(slide);
		});
		lesson.setAvatarId(lessonRequest.getSlides().get(0).getSrcSlide());
		if (preview)
			return lesson;
		return lessonRepository.save(lesson);

	}

	public LessonDetailResponse getLessonPreview(LessonRequest lessonRequest) {
		// TODO Auto-generated method stub
		Lesson lesson = createLesson(lessonRequest, true);

		return lessonToLessonResponse(lesson);

	}

	public PagedResponse<LessonResponse> getLessonByUserId(Long userId, UserPrincipal currentUser, int page, int size) {
		// TODO Auto-generated method stub
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "userId", userId));

		List<Lesson> lessons = lessonRepository.findByCreatedBy(userId, pageable);

		List<LessonResponse> lessonResponses = lessons.stream().map(lesson -> {
			LessonResponse lessonResponse = new LessonResponse();
			lessonResponse.setId(lesson.getId());
			lessonResponse.setName(lesson.getName());
			lessonResponse.setAvatarId(lesson.getAvatarId());
			lessonResponse.setCreatedBy(user);
			lessonResponse.setComments(commentRepository.countByLessonId(lesson.getId()));
			lessonResponse.setLikes(new ArrayList<>(lesson.getUsers()));
			lessonResponse.setCreatedAt(lesson.getCreatedAt().toString());

			return lessonResponse;
		}).collect(Collectors.toList());

		return new PagedResponse<>(lessonResponses, 1, lessons.size(), lessons.size(), 1, true);
	}

	public LessonDetailResponse getLessonById(Long lessonId, UserPrincipal currentUser) {
		// TODO Auto-generated method stub
		Lesson lesson = lessonRepository.findById(lessonId)
				.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", lessonId));

		return lessonToLessonResponse(lesson);
	}

	public LessonDetailResponse lessonToLessonResponse(Lesson lesson) {
		LessonDetailResponse lessonDetail = new LessonDetailResponse();
		System.out.print("id creator" + lesson.getCreatedBy());
		User creator = lesson.getCreatedBy() != null
				? userRepository.findById(lesson.getCreatedBy())
						.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", lesson.getCreatedBy()))
				: new User();

		List<Comment> comments = lesson.getId() != null
				? commentRepository.findByLessonIdOrderByCreateAtAsc(lesson.getId())
				: new ArrayList<>();
		lessonDetail.setLesson(lesson);
		lessonDetail.setCreatedBy(creator);
		lessonDetail.setComments(comments);

		return lessonDetail;
	}

	public void like(Long userId, Long lessonId) {
		// TODO Auto-generated method stub
		userRepository.like(userId, lessonId);

	}

	public void dislike(Long userId, Long lessonId) {
		// TODO Auto-generated method stub
		userRepository.dislike(userId, lessonId);

	}

	public List<Comment> getComments(Long lessonId) {
		List<Comment> comments = commentRepository.findByLessonIdOrderByCreateAtAsc(lessonId);
		return comments;
	}

	public void comment(Long userId, Long lessonId, String comment) {
		Comment newComment = new Comment();
		Lesson lesson = lessonRepository.findById(lessonId)
				.orElseThrow(() -> new ResourceNotFoundException("Lesson", "id", lessonId));
		User creator = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
		newComment.setText(comment);
		newComment.setUser(creator);
		newComment.setLesson(lesson);
		commentRepository.save(newComment);
	}

	public PagedResponse<LessonResponse> getLessonNotInClass(Long classId, UserPrincipal currentUser, int page,
			int size) {
		// TODO Auto-generated method stub
//        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");

		List<Lesson> lessons = lessonRepository.getLessonsNotInClass(classId, currentUser.getId());

		List<LessonResponse> lessonResponses = lessons.stream().map(lesson -> {

			User creator = userRepository.findById(lesson.getCreatedBy())
					.orElseThrow(() -> new ResourceNotFoundException("ClassRoom", "id", lesson.getCreatedBy()));
			LessonResponse lessonResponse = new LessonResponse();
			lessonResponse.setId(lesson.getId());
			lessonResponse.setName(lesson.getName());
			lessonResponse.setAvatarId(lesson.getAvatarId());
			lessonResponse.setCreatedBy(creator);
			lessonResponse.setComments(commentRepository.countByLessonId(lesson.getId()));
			lessonResponse.setLikes(new ArrayList<>(lesson.getUsers()));
			lessonResponse.setCreatedAt(lesson.getCreatedAt().toString());

			return lessonResponse;
		}).collect(Collectors.toList());

		return new PagedResponse<>(lessonResponses, 1, lessons.size(), lessons.size(), 1, true);
	}

	public void deleteLesson(Long lessonId, UserPrincipal currentUser) {
		Lesson lesson = lessonRepository.findById(lessonId)
				.orElseThrow(() -> new ResourceNotFoundException("Lesson", "id", lessonId));
		commentRepository.deleteByLessonId(lessonId);
		lessonRepository.deleteLikes(lessonId);
		classRoomRepository.deleteLessonAllClass(lessonId);
		lessonRepository.delete(lesson);
	}

	public void deleteLessonInClass(Long lessonId, Long classId, UserPrincipal currentUser) {
		Lesson lesson = lessonRepository.findById(lessonId)
				.orElseThrow(() -> new ResourceNotFoundException("Lesson", "id", lessonId));
		commentRepository.deleteByLessonId(lessonId);
		lessonRepository.deleteLikes(lessonId);
		classRoomRepository.deleteLessonInClass(classId, lessonId);
		lessonRepository.delete(lesson);
	}

	public Lesson editLesson(@Valid LessonRequest lessonRequest) {
		Lesson lesson = lessonRepository.findById(lessonRequest.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Lesson", "id", lessonRequest.getId()));
		List<Slide> listSlides = new ArrayList<Slide>();

		lesson.getSlides().clear();

		lessonRequest.getSlides().forEach(slideRequest -> {
			List<Note> listNotes = new ArrayList<Note>();
			Slide slide = slideRepository.findById(slideRequest.getId())
					.orElseThrow(() -> new ResourceNotFoundException("Lesson", "id", slideRequest.getId()));
			;

			slide.getNotes().clear();
			slideRequest.getNotes().forEach(noteRequest -> {
				String srcAudio = createImageAndVideo.getSrcAudio(noteRequest.getText(), noteRequest.getReaddingSpeed(),
						noteRequest.getVoiceType());
				Note note = null;
				if (noteRequest.getId() != null)
					note = noteRepository.findById(noteRequest.getId())
							.orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteRequest.getId()));
				else
					note = new Note();
				note.setText(noteRequest.getText());
				note.setVoiceType(noteRequest.getVoiceType());
				note.setReaddingSpeed(noteRequest.getReaddingSpeed());
				note.setSrcAudio(srcAudio);
				note.setStartTime(noteRequest.getStartTime());
				note.setEndTime(noteRequest.getEndTime());

				listNotes.add(note);
				slide.addNote(note);
			});
			slide.setSrcSlide(slideRequest.getSrcSlide());
			slide.setEndTime(slideRequest.getEndTime());
			slide.setStartTime(slideRequest.getStartTime());
			lesson.addSlide(slide);
		});
		return lessonRepository.save(lesson);

	}

}
