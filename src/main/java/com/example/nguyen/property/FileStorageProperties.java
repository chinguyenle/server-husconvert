package com.example.nguyen.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author nguyen
 *May 14, 2019
 */

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {

	private String uploadDir;

	public String getUploadDir() {
		return uploadDir;
	}

	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}
	
	
}
