package com.example.nguyen.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author nguyen
 *May 7, 2019
 */

@Entity
@Table(name = "slides")
public class Slide {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
    @Column(name = "src_avatar")
	private String srcSlide;
	
	@NotNull
    @Column(name = "end_time")
	private int endTime;
	
	@NotNull
    @Column(name = "start_time")
	private int startTime;
	
	@OneToMany(
			mappedBy = "slide",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			orphanRemoval = true
			)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 30)
	private List<Note> notes = new ArrayList<>();
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="lesson_id", nullable = false)
	private Lesson lesson;

	public Slide() {
		super();
	}

	public Slide(String srcSlide, @NotBlank int endTime, @NotBlank int startTime, List<Note> notes) {
		super();
		this.srcSlide = srcSlide;
		this.endTime = endTime;
		this.startTime = startTime;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSrcSlide() {
		return srcSlide;
	}

	public void setSrcSlide(String srcSlide) {
		this.srcSlide = srcSlide;
	}

	public int getEndTime() {
		return endTime;
	}

	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Lesson getLesson() {
		return lesson;
	}

	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}
	
	public void addNote(Note note) {
		notes.add(note);
		note.setSlide(this);
	}
	
	public void removeChoice(Note note) {
		notes.remove(note);
		note.setSlide(null);
	}
	
}
