package com.example.nguyen.model;

import java.time.LocalDateTime;


/**
 * @author nguyen
 *Jun 2, 2019
 */
public class ChatMessage {

	private MessageType type;
	private MessageLocalType localType;
    private String content;
    private String sender;
    private LocalDateTime dateTime=LocalDateTime.now();; 
    
    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE,
        TYPING
    }
    
    public enum MessageLocalType {
    	COMMENT,
    	NOTIFICATION
    }
    
    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
    
    public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public MessageLocalType getLocalType() {
		return localType;
	}

	public void setLocalType(MessageLocalType localType) {
		this.localType = localType;
	}
	
}
