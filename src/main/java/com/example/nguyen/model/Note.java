package com.example.nguyen.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author nguyen
 *May 7, 2019
 */
@Entity
@Table(name = "notes")
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	private String text;
	
	@NotBlank
    @Column(name = "voice_type")
	private String voiceType;
	
	@NotBlank
    @Column(name = "reading_speed")
	private String readdingSpeed;
	
	
    @Column(name = "scr_audio")
	private String srcAudio;
	
	@NotNull
    @Column(name = "start_time")
	private int startTime;
	
	@NotNull
    @Column(name = "end_time")
	private int endTime;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="slide_id", nullable = false)
	private Slide slide;

	public Note(String text,String voiceType, String readdingSpeed,
			 String srcAudio,int startTime, int endTime) {
		this.text = text;
		this.voiceType = voiceType;
		this.readdingSpeed = readdingSpeed;
		this.srcAudio = srcAudio;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	
	public Note() {
		super();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getVoiceType() {
		return voiceType;
	}

	public void setVoiceType(String voiceType) {
		this.voiceType = voiceType;
	}

	public String getReaddingSpeed() {
		return readdingSpeed;
	}

	public void setReaddingSpeed(String readdingSpeed) {
		this.readdingSpeed = readdingSpeed;
	}

	public Slide getSlide() {
		return slide;
	}

	public void setSlide(Slide slide) {
		this.slide = slide;
	}

	public String getSrcAudio() {
		return srcAudio;
	}

	public void setSrcAudio(String srcAudio) {
		this.srcAudio = srcAudio;
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public int getEndTime() {
		return endTime;
	}

	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}
	
	
	
}
