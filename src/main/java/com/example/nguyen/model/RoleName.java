package com.example.nguyen.model;

public enum RoleName {
	ROLE_USER,
	ROLE_TEACHER,
	ROLE_ADMIN
}
