package com.example.nguyen.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.example.nguyen.model.audit.UserDateAudit;

/**
 * @author nguyen
 *May 7, 2019
 */
@Entity
@Table(name = "class_room")
public class ClassRoom extends UserDateAudit {

	private static final long serialVersionUID = -223626738382580244L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
    @Column(name = "class_roomn_name")
	private String classRoomName;


    @Column(name = "avatar_id")
	private String avatarId;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@JoinTable(name = "classroom_lessons",
		joinColumns = @JoinColumn(name= "class_id"),
		inverseJoinColumns = @JoinColumn(name = "lesson_id"))
	private Set<Lesson> lessons = new  HashSet<>();
	

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "classroom_users",
		joinColumns = @JoinColumn(name= "class_id"),
		inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> users = new  HashSet<>();
	
	public ClassRoom() {
		
	}
	
	public ClassRoom(Long id, String classRoomName) {
		this.id = id;
		this.classRoomName = classRoomName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClassRoomName() {
		return classRoomName;
	}

	public void setClassRoomName(String classRoomName) {
		this.classRoomName = classRoomName;
	}
	
	
	public Set<Lesson> getLessons() {
		return lessons;
	}

	public void setLessons(Set<Lesson> lessons) {
		this.lessons = lessons;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public String getAvatarId() {
		return avatarId;
	}

	public void setAvatarId(String avatarId) {
		this.avatarId = avatarId;
	}

	
	
}
