package com.example.nguyen.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.example.nguyen.model.audit.UserDateAudit;

/**
 * @author nguyen
 *May 7, 2019
 */

@Entity
@Table(name = "lessons")
public class Lesson extends UserDateAudit {
	
	private static final long serialVersionUID = -6345759821233978499L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
    @Column(name = "name")
	private String name;
	
    @Column(name = "avatar_id")
	private String avatarId;
	
	@OneToMany(
			mappedBy = "lesson",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			orphanRemoval = true
			)
//	@Size(min = 2, max = 6)
	@Fetch(FetchMode.SELECT)
	private List<Slide> slides = new ArrayList<>();
	

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@JoinTable(name = "likes",
		joinColumns = @JoinColumn(name= "lessons_id"),
		inverseJoinColumns = @JoinColumn(name = "users_id"))
	private Set<User> users = new  HashSet<>();

	public String getAvatarId() {
		return avatarId;
	}
	

	public Lesson() {
		super();
	}


	public Lesson(String name) {
		super();
		this.name = name;
	}

	public void setAvatarId(String avatarId) {
		this.avatarId = avatarId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Slide> getSlides() {
		return slides;
	}

	public void setSlides(List<Slide> slides) {
		this.slides = slides;
	}
	
	public void addSlide(Slide slide) {
		slides.add(slide);
		slide.setLesson(this);
	}
	
	public void removeSlide(Slide slide) {
		slides.remove(slide);
		slide.setLesson(null);
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
	
}
