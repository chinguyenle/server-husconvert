package com.example.nguyen.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author nguyen
 *Jun 2, 2019
 */

@Entity
@Table(name = "join_request")
public class JoinRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="class_id", nullable = false)
//	@JsonIgnore
	private ClassRoom classRoom;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	
	@Enumerated(EnumType.STRING)
	private JoinRequestStatus status;

	

	public JoinRequest(ClassRoom classRoom, User user, JoinRequestStatus status) {
		super();
		this.classRoom = classRoom;
		this.user = user;
		this.status = status;
	}

	public JoinRequest() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public JoinRequestStatus getStatus() {
		return status;
	}

	public void setStatus(JoinRequestStatus status) {
		this.status = status;
	}

	
	
	
}
