package com.example.nguyen.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.Comment;

/**
 * @author nguyen
 *May 8, 2019
 */

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

	Optional<Comment> findById(Long id);
	@Query(value="select * from husconvert.comments where lesson_id =:id order by create_at desc ", nativeQuery = true)
	List<Comment> findByLessonIdOrderByCreateAtAsc(Long id);
	int countByLessonId(Long lessonId);
	
	void deleteByLessonId(Long lessonId);
	void deleteByUserId(Long userId);
	
}
