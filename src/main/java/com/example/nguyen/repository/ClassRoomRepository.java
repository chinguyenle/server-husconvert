package com.example.nguyen.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 7, 2019
 */

@Repository
@Transactional
public interface ClassRoomRepository extends JpaRepository<ClassRoom, Long> {

	Optional<ClassRoom> findById(Long classId);
	
	List<ClassRoom> findByCreatedBy(Long userId, Pageable pageable);
	
	long countByCreatedBy(Long userId);
	
	List<ClassRoom> findByIdIn(List<Long> classIds);
	
	List<ClassRoom> findByIdIn(List<Long> classIds, Sort sort);
	
	@Query(value="select * from husconvert.class_room cr where cr.id in (select cu.class_id from husconvert.classroom_users cu where cu.user_id = :userId)", nativeQuery = true)
	List<ClassRoom> findAllClassRoomHasUser(@Param("userId") Long userId);
	
	@Modifying
	@Query(value="delete from husconvert.classroom_users where husconvert.classroom_users.class_id = :classId and husconvert.classroom_users.user_id in :users", nativeQuery = true)
	void deleteUserInClass(@Param("classId") Long classId, @Param("users") List<Long> users);
	
	@Modifying
	@Query(value="delete from husconvert.classroom_lessons where husconvert.classroom_lessons.class_id = :classId and husconvert.classroom_lessons.lessons_id = :lessonId", nativeQuery = true)
	void deleteLessonInClass(@Param("classId") Long classId, @Param("lessonId") Long lessonId);
	
	@Modifying
	@Query(value="delete from husconvert.classroom_lessons where husconvert.classroom_lessons.lesson_id = :lessonId", nativeQuery = true)
	void deleteLessonAllClass(@Param("lessonId") Long lessonId);
	
	List<ClassRoom> findByClassRoomNameContainingIgnoreCase(String name);
	
	@Modifying
	@Query(value="delete from husconvert.classroom_users where husconvert.classroom_users.user_id = :userId", nativeQuery = true)
	void deleteUserInAllClass(@Param("userId") Long userId);
	
	void deleteById(Long classId);
	
	@Modifying
	@Query(value="delete from husconvert.classroom_users where husconvert.classroom_users.class_id = :classId", nativeQuery = true)
	void deleteClassUser(@Param("classId") Long classId);
	
	@Modifying
	@Query(value="delete from husconvert.classroom_lessons where husconvert.classroom_lessons.class_id = :classId ", nativeQuery = true)
	void deleteClassLesson(@Param("classId") Long classId);
}
