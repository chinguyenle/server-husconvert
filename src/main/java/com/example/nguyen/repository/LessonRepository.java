package com.example.nguyen.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 7, 2019
 */

@Repository
@Transactional
public interface LessonRepository extends JpaRepository<Lesson, Long> {

	Optional<Lesson> findById(Long lessonId);
	
	List<Lesson> findByCreatedBy(Long userId, Pageable pageable);
	
	long countByCreatedBy(Long userId);
	
	List<Lesson> findByIdIn(List<Long> lessonId);
	
	List<Lesson> findByIdIn(List<Long> lessonId, Sort sort);
	
	@Modifying
	@Query(value="delete from husconvert.likes where  husconvert.likes.lessons_id = :lessonId", nativeQuery = true)
	void deleteLikes(@Param("lessonId") Long lessonId);
	
	@Query(value="select * from husconvert.lessons ls where( ls.id not in (select cl.lesson_id from husconvert.classroom_lessons cl where cl.class_id = :classId) and ls.created_by=:userId) ORDER BY create_at DESC", nativeQuery = true)
	List<Lesson> getLessonsNotInClass(@Param("classId") Long classId, @Param("userId") Long userId );
	
	@Modifying
	@Query(value="delete from husconvert.likes where  husconvert.likes.users_id = :userId", nativeQuery = true)
	void deleteLikesByUserId(@Param("userId") Long userId);
}
