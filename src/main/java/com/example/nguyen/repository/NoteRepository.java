package com.example.nguyen.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.Note;

/**
 * @author nguyen
 *May 7, 2019
 */


@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

	Optional<Note> findById(Long noteId);
	
	List<Note> findByIdIn(List<Long> noteId);
	
	
}
