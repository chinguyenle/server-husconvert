package com.example.nguyen.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByEmail(String email);
	
	Optional<User> findByUsernameOrEmail(String username, String email);
	
	List<User> findByIdIn(List<Long> userIds);
	
	Optional<User> findByUsername(String username);
	
	Boolean existsByUsername(String username);
	
	Boolean existsByEmail(String email);
	
	@Query(value="select * from husconvert.users us where( 1 = (select ur.role_id from user_roles ur where ur.user_id=us.id)) and us.id not in (select cu.user_id from husconvert.classroom_users cu where cu.class_id = :classId) ORDER BY email DESC", nativeQuery = true)
	List<User> getAllUserNotInClass(@Param("classId") Long classId);
	
	@Modifying
	@Transactional
	@Query(value="insert into husconvert.likes (lessons_id, users_id) values(:lessonId, :userId)", nativeQuery = true)
	void like(@Param("userId") Long userId, @Param("lessonId") Long lessonId);
	
	@Modifying
	@Transactional
	@Query(value="delete from husconvert.likes where (users_id = :userId and lessons_id = :lessonId)", nativeQuery = true)
	void dislike(@Param("userId") Long userId, @Param("lessonId") Long lessonId);
	
	@Modifying
	@Transactional
	@Query(value="update husconvert.users set email= :email where id =:userId", nativeQuery = true)
	void updateEmail(@Param("userId") Long userId, @Param("email") String email);
	
	List<User> findByNameContainingIgnoreCase(String name);
}
