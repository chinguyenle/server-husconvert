package com.example.nguyen.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.Comment;
import com.example.nguyen.model.JoinRequest;

/**
 * @author nguyen
 *Jun 2, 2019
 */
@Repository
public interface JoinRequestRepository extends JpaRepository<JoinRequest, Long> {

	
//	@Modifying
	@Transactional
	@Query(value="Select count(join_request.id) from join_request where (user_id = :userId and class_id = :classId)", nativeQuery = true)
	int countRequest(@Param("classId") Long classId, @Param("userId") Long userId);
	
	@Transactional
	@Query(value="Select * from join_request where (class_id = :classId)", nativeQuery = true)
	List<JoinRequest> getListRequest(@Param("classId") Long classId);
	
	void deleteByUserId(Long userId);
	
	@Modifying
	@Transactional
	@Query(value="delete from join_request where (class_id = :classId)", nativeQuery = true)
	void deleteRequestClassId(@Param("classId") Long classId);
}
