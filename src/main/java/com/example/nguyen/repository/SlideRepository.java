package com.example.nguyen.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.Slide;

/**
 * @author nguyen
 *May 7, 2019
 */


@Repository
public interface SlideRepository extends JpaRepository<Slide, Long> {

	Optional<Slide> findById(Long slideId);
	
	List<Slide> findByIdIn(List<Long> slideIds);
	List<Slide> findByIdIn(List<Long> slidesIds, Sort sort);
}
