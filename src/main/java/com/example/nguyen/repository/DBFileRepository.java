package com.example.nguyen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.DBFile;

/**
 * @author nguyen
 *May 10, 2019
 */
@Repository
public interface DBFileRepository extends JpaRepository<DBFile, String> {

}
