package com.example.nguyen.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.nguyen.model.Note;
import com.example.nguyen.model.Notification;

/**
 * @author nguyen
 *May 21, 2019
 */
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {
	
	@Query(value="select * from husconvert.notification no where( no.class_room_id = :classRoomId) ORDER BY create_at DESC", nativeQuery = true)
	List<Notification> findByClassRoomId(Long classRoomId);

}
