package com.example.nguyen.payload;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.example.nguyen.model.Slide;

/**
 * @author nguyen
 *May 28, 2019
 */
public class LessonRequest {
	
	private Long id;
	
	private String name;
	
	private String avatarId;
	
	@Valid
	private List<SlideRequest> slides = new ArrayList<>();

	public String getAvatarId() {
		return avatarId;
	}

	public void setAvatarId(String avatarId) {
		this.avatarId = avatarId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SlideRequest> getSlides() {
		return slides;
	}

	public void setSlides(List<SlideRequest> slides) {
		this.slides = slides;
	}
	
}
