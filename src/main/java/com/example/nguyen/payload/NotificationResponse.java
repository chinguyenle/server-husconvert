package com.example.nguyen.payload;

import java.time.Instant;

import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 30, 2019
 */
public class NotificationResponse {

	private Long id;
	private String createdBy;
	private Instant createdAt;
	private String text;
	private Long classId;
	
	public NotificationResponse() {
		super();
	}
	public NotificationResponse(Long id, String createdBy, Instant createdAt, String text, Long classId) {
		this.id = id;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.text = text;
		this.classId = classId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Instant getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Instant createdAt) {
		this.createdAt = createdAt;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Long getClassId() {
		return classId;
	}
	public void setClassId(Long classId) {
		this.classId = classId;
	}
	
	
	
}
