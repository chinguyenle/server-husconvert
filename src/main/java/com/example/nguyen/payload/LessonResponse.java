package com.example.nguyen.payload;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.example.nguyen.model.Comment;
import com.example.nguyen.model.Slide;
import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 29, 2019
 */
public class LessonResponse {

	private Long id;
	private String name;
	private String avatarId;
//	private List<Slide> slides;
	private User createdBy;
	private int comments;
	private List<User> likes;
	private String createdAt;
	public LessonResponse() {
		super();
	}
	public LessonResponse(Long id, String name, String avatarId, User createdBy, int comments, List<User> likes, String createdAt) {
		super();
		this.id = id;
		this.name = name;
		this.avatarId = avatarId;
		this.createdBy = createdBy;
		this.comments = comments;
		this.likes = likes;
		this.createdAt = createdAt;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAvatarId() {
		return avatarId;
	}
	public void setAvatarId(String avatarId) {
		this.avatarId = avatarId;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public int getComments() {
		return comments;
	}
	public void setComments(int comments) {
		this.comments = comments;
	}
	
	public List<User> getLikes() {
		return likes;
	}
	public void setLikes(List<User> likes) {
		this.likes = likes;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
}
