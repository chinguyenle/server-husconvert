package com.example.nguyen.payload;

import javax.validation.constraints.NotNull;

/**
 * @author nguyen
 *May 5, 2019
 */
public class VoteRequest {

	@NotNull
	private Long choiceId;
	
	public Long getChoiceId() {
		return choiceId;
	}
	
	public void setChoiceId(Long choiceId) {
		this.choiceId = choiceId;
	}
}
