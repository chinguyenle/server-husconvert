package com.example.nguyen.payload;

import com.example.nguyen.model.User;

/**
 * @author nguyen
 *Jun 2, 2019
 */
public class JoinRequestResponse {
	private Long id;
	private Long classId;
	private User userResquest;
	public JoinRequestResponse(Long id, Long classId, User userResquest) {
		super();
		this.id = id;
		this.classId = classId;
		this.userResquest = userResquest;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getClassId() {
		return classId;
	}
	public void setClassId(Long classId) {
		this.classId = classId;
	}
	public User getUserResquest() {
		return userResquest;
	}
	public void setUserResquest(User userResquest) {
		this.userResquest = userResquest;
	}
	
	

}
