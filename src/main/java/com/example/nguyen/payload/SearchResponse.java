package com.example.nguyen.payload;

import java.util.List;

import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.User;

/**
 * @author nguyen
 *Jun 1, 2019
 */
public class SearchResponse {
	private List<User> users;
	private List<ClassResponse> classes;
	private boolean success;
	public SearchResponse(List<User> users, List<ClassResponse> classes, boolean success) {
		super();
		this.users = users;
		this.classes = classes;
		this.success = success;
	}
	public SearchResponse() {
		super();
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public List<ClassResponse> getClasses() {
		return classes;
	}
	public void setClasses(List<ClassResponse> classes) {
		this.classes = classes;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	
}
