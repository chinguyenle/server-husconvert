package com.example.nguyen.payload;

import java.util.List;

import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 14, 2019
 */
public class AddUserToClassRequest {
	private List<User> listUser;

	public List<User> getListUser() {
		return listUser;
	}

	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}
	
	
}
