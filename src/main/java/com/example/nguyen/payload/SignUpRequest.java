package com.example.nguyen.payload;


import java.io.File;
import java.time.Instant;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author nguyen
 *May 2, 2019
 */
public class SignUpRequest {

	@NotBlank
	@Size(min = 4, max = 40)
	private String name;
	
	
	@NotBlank
	@Size(min = 4, max = 40)
	private String username;
	
	@NotBlank
	@Size(max = 40)
	private String email;
	
	@NotBlank
	@Size(min = 6, max = 20)
	private String password;

	@NotBlank
	private String dateOfBirth;
	
	private MultipartFile avatar;
	
	public SignUpRequest(@NotBlank @Size(min = 4, max = 40) String name,
			@NotBlank @Size(min = 4, max = 40) String username, @NotBlank @Size(max = 40) String email,
			@NotBlank @Size(min = 6, max = 20) String password, @NotBlank String dateOfBirth) {
		super();
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public MultipartFile getAvatar() {
		return avatar;
	}

	public void setAvatar(MultipartFile avatar) {
		this.avatar = avatar;
	}

	
}
