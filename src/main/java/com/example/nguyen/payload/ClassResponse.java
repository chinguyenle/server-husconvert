package com.example.nguyen.payload;

import java.time.Instant;
import java.util.List;

import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 8, 2019
 */
public class ClassResponse {

	private Long id;
	private ClassRoom classRoom;
	private User createdBy;
	private boolean sendRequestToJoin;
	private List<JoinRequestResponse> listRequestToJoin;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public ClassRoom getClassRoom() {
		return classRoom;
	}
	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

	
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public boolean isSendRequestToJoin() {
		return sendRequestToJoin;
	}
	public void setSendRequestToJoin(boolean sendRequestToJoin) {
		this.sendRequestToJoin = sendRequestToJoin;
	}
	public List<JoinRequestResponse> getListRequestToJoin() {
		return listRequestToJoin;
	}
	public void setListRequestToJoin(List<JoinRequestResponse> listRequestToJoin) {
		this.listRequestToJoin = listRequestToJoin;
	}

	
	
	
}
