package com.example.nguyen.payload;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.Note;

/**
 * @author nguyen
 *May 28, 2019
 */
public class SlideRequest {

	private Long id;
	
	
	private String srcSlide;
	
	private int endTime;
	
	private int startTime;

	private List<NoteRequest> notes = new ArrayList<>();
	
	private Lesson lesson;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSrcSlide() {
		return srcSlide;
	}

	public void setSrcSlide(String srcSlide) {
		this.srcSlide = srcSlide;
	}

	public int getEndTime() {
		return endTime;
	}

	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public List<NoteRequest> getNotes() {
		return notes;
	}

	public void setNotes(List<NoteRequest> notes) {
		this.notes = notes;
	}

	public Lesson getLesson() {
		return lesson;
	}

	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}
	
	
	
}
