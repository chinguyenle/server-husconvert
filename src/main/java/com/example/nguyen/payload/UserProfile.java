package com.example.nguyen.payload;

import java.time.Instant;
import java.util.List;

import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 5, 2019
 */
public class UserProfile {

	private Long id;
	private User user;
	public List<ClassResponse> joinedClasses;
	
	
	public UserProfile(Long id, User user, List<ClassResponse> joinedClasses) {
		super();
		this.id = id;
		this.user = user;
		this.joinedClasses = joinedClasses;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ClassResponse> getJoinedClasses() {
		return joinedClasses;
	}
	public void setJoinedClasses(List<ClassResponse> joinedClasses) {
		this.joinedClasses = joinedClasses;
	}
	
	
}
