package com.example.nguyen.payload;

import java.util.List;

import com.example.nguyen.model.Comment;

/**
 * @author nguyen
 *May 29, 2019
 */
public class CommentResponse {

	private Boolean success;
	private List<Comment> comments;
	
	public CommentResponse(Boolean success, List<Comment> comments) {

		this.success = success;
		this.comments = comments;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	
}
