package com.example.nguyen.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.example.nguyen.model.Slide;

/**
 * @author nguyen
 *May 28, 2019
 */
public class NoteRequest {


	private Long id;
	
	
	private String text;
	
	@NotBlank
	private String voiceType;
	
	@NotBlank
	private String readdingSpeed;
	
	
	private String srcAudio;
	
	@NotNull
	private int startTime;
	
	@NotNull
	private int endTime;
	
	private Slide slide;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getVoiceType() {
		return voiceType;
	}

	public void setVoiceType(String voiceType) {
		this.voiceType = voiceType;
	}

	public String getReaddingSpeed() {
		return readdingSpeed;
	}

	public void setReaddingSpeed(String readdingSpeed) {
		this.readdingSpeed = readdingSpeed;
	}

	public Slide getSlide() {
		return slide;
	}

	public void setSlide(Slide slide) {
		this.slide = slide;
	}

	public String getSrcAudio() {
		return srcAudio;
	}

	public void setSrcAudio(String srcAudio) {
		this.srcAudio = srcAudio;
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public int getEndTime() {
		return endTime;
	}

	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}
	
	
}
