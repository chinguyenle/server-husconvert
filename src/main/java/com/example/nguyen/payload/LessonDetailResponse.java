package com.example.nguyen.payload;

import java.util.List;

import com.example.nguyen.model.Comment;
import com.example.nguyen.model.Lesson;
import com.example.nguyen.model.User;

/**
 * @author nguyen
 *May 29, 2019
 */
public class LessonDetailResponse {
//	private long id;
	private Lesson lesson;
	private User createdBy;
	private List<Comment> comments;
//	private String cre
	public LessonDetailResponse( Lesson lesson, User createdBy, List<Comment> comments) {
		super();
		this.lesson = lesson;
		this.createdBy = createdBy;
		this.comments = comments;
	}
	public LessonDetailResponse() {
		super();
	}
	public Lesson getLesson() {
		return lesson;
	}
	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	

}
