package com.example.nguyen.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author nguyen
 *May 10, 2019
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class MyFileNotFoundException extends AppException {

	private static final long serialVersionUID = 4585273050547770339L;

	public MyFileNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MyFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
