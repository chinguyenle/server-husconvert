package com.example.nguyen.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.nguyen.model.DBFile;
import com.example.nguyen.model.Lesson;
import com.example.nguyen.payload.ApiResponse;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.CommentResponse;
import com.example.nguyen.payload.JwtAuthenticationResponse;
import com.example.nguyen.payload.LessonDetailResponse;
import com.example.nguyen.payload.LessonRequest;
import com.example.nguyen.payload.LessonResponse;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.payload.PollRequest;
import com.example.nguyen.payload.PollResponse;
import com.example.nguyen.repository.UserRepository;
import com.example.nguyen.security.CurrentUser;
import com.example.nguyen.security.UserPrincipal;
import com.example.nguyen.service.ConvertMultiPathToFile;
import com.example.nguyen.service.CreateImageAndVideo;
import com.example.nguyen.service.DBFileStorageService;
import com.example.nguyen.service.LessonService;
import com.example.nguyen.service.PPTStorageService;
import com.example.nguyen.util.AppConstants;

/**
 * @author nguyen May 14, 2019
 */
@RestController
@RequestMapping("/api/lesson")
public class LessonController {

	@Autowired
	private DBFileStorageService dbFileStorageService;

	@Autowired
	private LessonService lessonService;

	@Autowired
	private PPTStorageService pptStorageService;

	@Autowired
	private CreateImageAndVideo createImageAndVideo;
	
	@Autowired
	private UserRepository userRepository;

	@PostMapping("/initLesson")
	public ResponseEntity<?> createLesson(@CurrentUser UserPrincipal currentUser,
			@RequestParam("pptFile") MultipartFile pptFile) throws Exception {

		System.out.println(" file ppt  -----" + pptFile.getOriginalFilename());
		Lesson lesson = lessonService.initLesson(currentUser, pptFile);

		return new ResponseEntity<>(lesson, HttpStatus.OK);

	}

	@PostMapping("/createLesson")
	public ResponseEntity<?> createLesson(@Valid @RequestBody LessonRequest lesson) {

		lessonService.createLesson(lesson, false);
		return ResponseEntity.ok(new ApiResponse(true, "Lesson is created successfully"));

	}
	@PostMapping("/previewLesson")
	public LessonDetailResponse getPreviewLesson(@Valid @RequestBody LessonRequest lesson) {

//		lessonService.getLessonPreview(lesson);
		return lessonService.getLessonPreview(lesson);

	}

	@GetMapping("/user={userId}")
	public PagedResponse<LessonResponse> getPollById(@CurrentUser UserPrincipal currentUser,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
			@PathVariable Long userId) {
		return lessonService.getLessonByUserId(userId, currentUser, page, size);
	}
	
	@GetMapping("/id={lessonId}")
    public LessonDetailResponse getClassRoomById(@CurrentUser UserPrincipal currentUser, @PathVariable Long lessonId) {
        return lessonService.getLessonById(lessonId, currentUser);
    }
	
	@PostMapping("/delete/id={lessonId}")
    public ResponseEntity<?> deleteLesson(@CurrentUser UserPrincipal currentUser, @PathVariable Long lessonId) {
         lessonService.deleteLesson(lessonId, currentUser);
         return ResponseEntity.ok(new ApiResponse(true, "Lesson is deleted successfully"));
    }
	

	@PostMapping("/editLesson")
	public ResponseEntity<?> editLesson(@Valid @RequestBody LessonRequest lessonRequest) {

		Lesson lesson = lessonService.editLesson(lessonRequest);
		System.out.print("edittt");
		return ResponseEntity.ok(new ApiResponse(true, "Lesson is edited successfully"));

	}
	
	@PostMapping("/like")
	public ResponseEntity<?> like(@CurrentUser UserPrincipal currentUser,
			@RequestParam("userId") Long userId,
			@RequestParam("lessonId") Long lessonId) {

//		userRepository.like(userId, lessonId);
		lessonService.like(userId, lessonId);
		
		return ResponseEntity.ok(new ApiResponse(true, "You are like this lesson"));
	}
	@PostMapping("/dislike")
	public ResponseEntity<?> unlike(@CurrentUser UserPrincipal currentUser,
			@RequestParam("userId") Long userId,
			@RequestParam("lessonId") Long lessonId) {

//		userRepository.dislike(userId, lessonId);
		lessonService.dislike(userId, lessonId);
		
		return ResponseEntity.ok(new ApiResponse(true, "You are dislike this lesson"));
	}
	
	@PostMapping("/postComment")
	public ResponseEntity<?> comment(@CurrentUser UserPrincipal currentUser,
			@RequestParam("userId") Long userId,
			@RequestParam("lessonId") Long lessonId,
			@RequestParam("text") String text) {

		lessonService.comment(userId, lessonId, text);
		
		
		return ResponseEntity.ok(new CommentResponse(true, lessonService.getComments(lessonId)));
	}
	
	@PostMapping("/getComment")
	public ResponseEntity<?> getComment(@CurrentUser UserPrincipal currentUser,
			@RequestParam("lessonId") Long lessonId) {
		return ResponseEntity.ok(new CommentResponse(true, lessonService.getComments(lessonId)));
	}
	
	
	@GetMapping("/notInClass={classId}")
	public PagedResponse<LessonResponse> getLessonNotInClass(@CurrentUser UserPrincipal currentUser,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
			@PathVariable Long classId) {
		return lessonService.getLessonNotInClass(classId, currentUser, page, size);
	}

}
