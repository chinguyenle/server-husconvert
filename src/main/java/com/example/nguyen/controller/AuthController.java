package com.example.nguyen.controller;

import com.example.nguyen.exception.AppException;
import com.example.nguyen.model.DBFile;
import com.example.nguyen.model.Role;
import com.example.nguyen.model.RoleName;
import com.example.nguyen.model.User;
import com.example.nguyen.payload.ApiResponse;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.JwtAuthenticationResponse;
import com.example.nguyen.payload.LoginRequest;
import com.example.nguyen.payload.SignUpRequest;
import com.example.nguyen.payload.UserProfile;
import com.example.nguyen.repository.RoleRepository;
import com.example.nguyen.repository.UserRepository;
import com.example.nguyen.security.JwtTokenProvider;
import com.example.nguyen.security.UserPrincipal;
import com.example.nguyen.service.ClassService;
import com.example.nguyen.service.DBFileStorageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Collections;
import java.util.List;

/**
 * Created by nguyen
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private DBFileStorageService dBFileStorageService;

	@Autowired
	private ClassService classService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }


    @PostMapping("/up")
    public ResponseEntity<?> createAvatar(@RequestParam("avatar") MultipartFile avatar) {
    	DBFile dbFile = null;
    	if(avatar != null) {
    		dbFile = dBFileStorageService.storeFile(avatar);
    	}
    	try {

            System.out.println("file name      "+ dbFile.getId());

            return new ResponseEntity<>(dbFile.getId(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(
    		@RequestParam(value="avatar", required = false ) MultipartFile avatar,
    		@RequestParam("name") String name,
    		@RequestParam("email") String email,
    		@RequestParam("username") String username,
    		@RequestParam("dateOfBirth") String dateOfBirth,
    		@RequestParam("password") String password ) {
    	SignUpRequest signUpRequest = new SignUpRequest(name, username, email, password, dateOfBirth);
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    	DBFile dbFile = null;
    	if(avatar != null) {
    		dbFile = dBFileStorageService.storeFile(avatar);
    	}
//    	

        String timestamp = signUpRequest.getDateOfBirth();//"2016-02-16 11:00:02";
        TemporalAccessor temporalAccessor = formatter.parse(timestamp);
        LocalDateTime localDateTime = LocalDateTime.from(temporalAccessor);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        Instant dateOfBirthInstant = Instant.from(zonedDateTime);
        
    	if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        System.out.print("----------");
        System.out.print(signUpRequest);

        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPassword(), dateOfBirthInstant);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if(dbFile!= null) {
        	user.setAvatarId(dbFile.getId());
        }

        System.out.print(user);
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));
        // Role teacherRole = roleRepository.findByName(RoleName.ROLE_TEACHER)
        //         .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }
}
