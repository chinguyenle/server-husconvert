package com.example.nguyen.controller;

import java.net.URI;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.nguyen.exception.AppException;
import com.example.nguyen.exception.ResourceNotFoundException;
import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.DBFile;
import com.example.nguyen.model.Role;
import com.example.nguyen.model.RoleName;
import com.example.nguyen.model.User;
import com.example.nguyen.payload.ApiResponse;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.LessonResponse;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.payload.PollResponse;
import com.example.nguyen.payload.SignUpRequest;
import com.example.nguyen.payload.UserIdentityAvailability;
import com.example.nguyen.payload.UserProfile;
import com.example.nguyen.payload.UserSummary;
import com.example.nguyen.repository.PollRepository;
import com.example.nguyen.repository.RoleRepository;
import com.example.nguyen.repository.UserRepository;
import com.example.nguyen.repository.VoteRepository;
import com.example.nguyen.security.CurrentUser;
import com.example.nguyen.security.JwtTokenProvider;
import com.example.nguyen.security.UserPrincipal;
import com.example.nguyen.service.ClassService;
import com.example.nguyen.service.DBFileStorageService;
import com.example.nguyen.service.PollService;
import com.example.nguyen.service.UserService;
import com.example.nguyen.util.AppConstants;

/**
 * @author nguyen
 *May 6, 2019
 */

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PollRepository pollRepository;
	
	@Autowired
	private VoteRepository voteRepository;

    @Autowired
    RoleRepository roleRepository;
	
	@Autowired
	private PollService pollService;

	@Autowired
	private ClassService classService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private DBFileStorageService dBFileStorageService;
	

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    
    @GetMapping("/user/all")
    public PagedResponse<User> getAllUser(@CurrentUser UserPrincipal currentUser) {
    	return userService.getAllUser();
    }
    @GetMapping("/user/me")
    public UserProfile getCurrentUser(@CurrentUser UserPrincipal currentUser) {
    	return userService.getUserProfile(currentUser.getId());
    }
    
    @GetMapping("/user/checkUsernameAvailability")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
    	Boolean isAvailable = !userRepository.existsByUsername(username);
    	return new UserIdentityAvailability(isAvailable);
    }
    
    @GetMapping("/user/checkEmailAvailability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
    	Boolean isAvailable = !userRepository.existsByEmail(email);
    	return new UserIdentityAvailability(isAvailable);
    }
    
    @GetMapping("/user/id={id}")
    public UserProfile getUserProfileById(@PathVariable(value = "id") Long id,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
    	User user = userRepository.findById(id)
    			.orElseThrow(() -> new ResourceNotFoundException("User", "username", id));
    	
    	List<ClassResponse> joinedClasses = classService.getMyClassRoombyUser(user.getId(), page, size).getContent();

    	UserProfile userProfile = new UserProfile(user.getId(), user, joinedClasses);
    	
    	
    	return userProfile;
    }
    @GetMapping("/users/{username}")
    public UserProfile getUserProfile(@PathVariable(value = "username") String username,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
    	User user = userRepository.findByUsername(username)
    			.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
    	
    	List<ClassResponse> joinedClasses = classService.getMyClassRoombyUser(user.getId(), page, size).getContent();

    	UserProfile userProfile = new UserProfile(user.getId(), user, joinedClasses);
    	
    	
    	return userProfile;
    }
    

    @GetMapping("/users/{userId}")
    public UserProfile editProfile(@Valid @RequestBody UserProfile UserProfileRequest,@PathVariable(value = "userId") Long userId) {

    	User user = userRepository.findById(userId)
    			.orElseThrow(() -> new ResourceNotFoundException("User", "username", userId));
    	
    	List<ClassResponse> joinedClasses = classService.getMyClassRoombyUser(user.getId(), 30, 30).getContent();

    	UserProfile userProfile = new UserProfile(user.getId(), user, joinedClasses);
    	
    	return userProfile;
    }
    
    @PostMapping("/user/checkpassword")
    public ResponseEntity<?> checkCurrentPass(@CurrentUser UserPrincipal currentUser, 
    		@RequestParam(value = "userId") Long userId, 
    		@RequestParam(value = "currentPassword") String currentPassword) {
    	User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getId()));
		System.out.println(" cureent "+user.getPassword() );
		System.out.println(" new "+passwordEncoder.encode("123") );
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(),currentPassword )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

		System.out.println(" check "+passwordEncoder.matches(passwordEncoder.encode("123"), "123") );
    	if(user.getPassword() == passwordEncoder.encode(currentPassword)) {
			return ResponseEntity.ok(new ApiResponse(true, "Current password is right"));
    	}
 
			return ResponseEntity.ok(new ApiResponse(false, "Current password is wrong")); 
    }
    
    @PostMapping("/user/edit")
    public ResponseEntity<?> editUser(
    		@CurrentUser UserPrincipal currentUser,
    		@RequestParam(value="userId", required= false) Long id,
    		@RequestParam(value="avatar", required = false ) MultipartFile avatar,
    		@RequestParam("name") String name,
    		@RequestParam("email") String email,
    		@RequestParam(value="role", required = false) String role,
    		@RequestParam("dateOfBirth") String dateOfBirth,
    		@RequestParam("newPassword") String newPassword ) {

    	User userRequest = userRepository.findById(currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getId()));
    	
    	User user = userRepository.findById(id!=null?id:currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", id!=null?id:currentUser.getId()));
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    	DBFile dbFile = null;
//    	

        String timestamp = dateOfBirth;//"2016-02-16 11:00:02";
        TemporalAccessor temporalAccessor = formatter.parse(timestamp);
        LocalDateTime localDateTime = LocalDateTime.from(temporalAccessor);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        Instant dateOfBirthInstant = Instant.from(zonedDateTime);

//        System.out.println("----------" +user.getEmail()+"--------"+ newPassword + (newPassword !=""&& newPassword!=null && newPassword.length() >= 6));
    	if(!user.getEmail().equals(email)) {
    		if(userRepository.existsByEmail(email)) {
                return ResponseEntity.ok(new ApiResponse(false, "Email Address already in use!"));
            }
    		else {
    			try {

                	userRepository.updateEmail(id, email);
					
				} catch (Exception e) {
					return ResponseEntity.ok(new ApiResponse(false, "Email: "+e.getMessage()));
				}
            }
    	}

    	System.out.print("role ------------ "+id+"  "+user.getEmail());
        

    	if(avatar!=null) {
    		dbFile = dBFileStorageService.storeFile(avatar);

        	user.setAvatarId(dbFile.getId());
    	}
    	
    	if(newPassword !=""&& newPassword!=null && newPassword.length() >= 6) {
            user.setPassword(passwordEncoder.encode(newPassword));
    	}
    	if(role !=""&& role!=null && role.length()> 0) {
    		if(userRequest.getRoles().iterator().next().getName() ==  RoleName.ROLE_ADMIN&&userRequest.getRoles().iterator().next().getName() !=  RoleName.ROLE_ADMIN) {
	    		Role userRole = roleRepository.findByName(role.equals(RoleName.ROLE_TEACHER.name())?RoleName.ROLE_TEACHER:RoleName.ROLE_USER)
	                    .orElseThrow(() -> new AppException("User Role not set."));
	    		user.getRoles().clear();
	    		user.getRoles().add(userRole);
    		}
    	}

        user.setName(name);
//        user.setEmail(email);
        user.setDateOfBirth(dateOfBirthInstant);
        

        User result = userRepository.save(user);

    	
    	List<ClassResponse> joinedClasses = classService.getMyClassRoombyUser(result.getId(), 1, 1).getContent();

    	UserProfile userProfile = new UserProfile(result.getId(), result, joinedClasses); 
    	
        return ResponseEntity.ok(userProfile);
    }
    
    @PostMapping("/user/delete/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteUser(@CurrentUser UserPrincipal currentUser, 
    		@PathVariable(value = "userId") Long userId) {
    	userService.deleteUser(userId);
 
			return ResponseEntity.ok(new ApiResponse(true, "User is deleted")); 
    }
    
    @PostMapping("/user/resetPassword/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> resetPasswordUser(@CurrentUser UserPrincipal currentUser, 
    		@PathVariable(value = "userId") Long userId) {
    	User userRequest = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username",userId));
    	userRequest.setPassword(passwordEncoder.encode("123456"));
    	userRepository.save(userRequest);
			return ResponseEntity.ok(new ApiResponse(true, "Information is changed")); 
    }
    
    
}
