package com.example.nguyen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.nguyen.payload.ApiResponse;
import com.example.nguyen.payload.CommentResponse;
import com.example.nguyen.payload.LessonResponse;
import com.example.nguyen.payload.NotificationResponse;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.repository.NotificationRepository;
import com.example.nguyen.security.CurrentUser;
import com.example.nguyen.security.UserPrincipal;
import com.example.nguyen.service.ClassService;
import com.example.nguyen.service.DBFileStorageService;
import com.example.nguyen.service.LessonService;
import com.example.nguyen.service.NotificationService;
import com.example.nguyen.service.PPTStorageService;
import com.example.nguyen.util.AppConstants;

/**
 * @author nguyen May 21, 2019
 */

@RestController
@RequestMapping("/api/notification")
public class NotificationController {
	@Autowired
	private DBFileStorageService dbFileStorageService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private NotificationRepository nofiNotificationRepository;

	@PostMapping("/createNofitication")
	public ResponseEntity<?> createNotification(@CurrentUser UserPrincipal currentUser,
			@RequestParam("userId") Long userId, @RequestParam("classId") Long classId,
			@RequestParam("text") String text) {
		notificationService.createNotification(userId, classId, text);

		return ResponseEntity.ok(new ApiResponse(true, "notification is created"));
	}

	@GetMapping("/classId={classId}")
	public PagedResponse<NotificationResponse> getNotificationOfClass(@CurrentUser UserPrincipal currentUser,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
			@PathVariable Long classId) {

		return notificationService.getNotificationOfClass(classId, page, size);
		
	}
}
