package com.example.nguyen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.nguyen.model.DBFile;
import com.example.nguyen.service.DBFileStorageService;

/**
 * @author nguyen
 *May 12, 2019
 */
@RestController
@RequestMapping("/api/file")
public class FileController {

	@Autowired
	private DBFileStorageService dbFileStorageService;
	
	@GetMapping("/fileId={fileId}")
	public ResponseEntity<?> getFile(@PathVariable String fileId){

		DBFile dbFile = dbFileStorageService.getFile(fileId);
		return ResponseEntity
				.ok()
				.contentType(MediaType.parseMediaType(dbFile.getFileType()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+dbFile.getFileName())
				.body(new ByteArrayResource(dbFile.getData()));
	}
	
}
