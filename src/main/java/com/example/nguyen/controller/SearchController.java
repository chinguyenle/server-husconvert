package com.example.nguyen.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.nguyen.model.User;
import com.example.nguyen.exception.ResourceNotFoundException;
import com.example.nguyen.model.Role;
import com.example.nguyen.model.RoleName;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.LessonResponse;
import com.example.nguyen.payload.SearchResponse;
import com.example.nguyen.repository.ClassRoomRepository;
import com.example.nguyen.repository.UserRepository;

/**
 * @author nguyen
 *Jun 1, 2019
 */

@RestController
@RequestMapping("/api/search")
public class SearchController {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ClassRoomRepository classRoomRepository;
	
	
	@GetMapping
	public ResponseEntity<?> search(@RequestParam("keyWord") String keyWord) {
		List<User> userTeachers = new ArrayList<User>();
		List<ClassResponse> classResponses = new ArrayList<ClassResponse>();
		userRepository.findByNameContainingIgnoreCase(keyWord)
				.forEach(user -> {
			List<Role> listRole = new ArrayList<Role>(user.getRoles());
			if(listRole.get(0).getName() == RoleName.ROLE_TEACHER )
				userTeachers.add(user);
			
		});
		classRoomRepository.findByClassRoomNameContainingIgnoreCase(keyWord).forEach(classroom ->{
			ClassResponse clrp = new ClassResponse();
			User user = userRepository.findById(classroom.getCreatedBy())
	                .orElseThrow(() -> new ResourceNotFoundException("User", "userId", classroom.getCreatedBy()));
			clrp.setId(classroom.getId());
			clrp.setClassRoom(classroom);
			clrp.setCreatedBy(user);
			classResponses.add(clrp);
		});
		SearchResponse searchResponse = new SearchResponse(
				userTeachers, 
				classResponses, 
				true);
		
		return new ResponseEntity<SearchResponse>(searchResponse, HttpStatus.OK);
	}

}
