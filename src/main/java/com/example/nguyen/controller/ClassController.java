package com.example.nguyen.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.nguyen.model.ClassRoom;
import com.example.nguyen.model.Poll;
import com.example.nguyen.model.User;
import com.example.nguyen.payload.AddUserToClassRequest;
import com.example.nguyen.payload.ApiResponse;
import com.example.nguyen.payload.ClassRoomRequest;
import com.example.nguyen.payload.ClassResponse;
import com.example.nguyen.payload.PagedResponse;
import com.example.nguyen.payload.PollRequest;
import com.example.nguyen.payload.PollResponse;
import com.example.nguyen.payload.VoteRequest;
import com.example.nguyen.repository.ClassRoomRepository;
import com.example.nguyen.repository.CommentRepository;
import com.example.nguyen.repository.LessonRepository;
import com.example.nguyen.repository.NoteRepository;
import com.example.nguyen.security.CurrentUser;
import com.example.nguyen.security.UserPrincipal;
import com.example.nguyen.service.ClassService;
import com.example.nguyen.util.AppConstants;

/**
 * @author nguyen
 *May 8, 2019
 */

@RestController
@RequestMapping("/api/classes")
public class ClassController {


	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private NoteRepository noteRepository;
	
	@Autowired
	private LessonRepository lessonRepository;
	
	@Autowired
	private ClassRoomRepository classRoomRepository;
	
	@Autowired
	private ClassService classService;
	
//	@GetMapping
//	public PagedResponse<ClassResponse> getAllClasses(@CurrentUser UserPrincipal currentUser,
//            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
//            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
//
//        // Retrieve Polls
//		System.out.println("get all class");
//        
//        return classService.getAllClasses(currentUser, page, size);
//	}
	
	@PostMapping
    @PreAuthorize("hasRole('TEACHER')")
    public ResponseEntity<ClassResponse> createClassRoom(
    		@RequestParam(value="avatar", required = false ) MultipartFile avatar,
    		@RequestParam("className") String className,
    		@CurrentUser UserPrincipal currentUser) {
		System.out.println("params" + className +"  --------    "+ currentUser.getName());
		
		ClassResponse newClass = classService.createClassRoom(className, avatar, currentUser);
		
        return new ResponseEntity<ClassResponse>(newClass, HttpStatus.OK);
    }
	
	@GetMapping("/{classId}")
    public  ClassResponse getClassRoomById(@CurrentUser UserPrincipal currentUser, @PathVariable Long classId) {
        return classService.getClassById(classId, currentUser);
    }
	
	
	@GetMapping("/myClass")
    public PagedResponse<ClassResponse> getMyClassRoom(@CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return classService.getMyClassRoombyUser(currentUser.getId(), page, size);
    }
	
	@GetMapping("/userNotIn-classId={classId}")
	public PagedResponse<User> getUsersNotInClass(@PathVariable(value = "classId") Long classId) {
		
		return classService.getUsersNotInClass(classId);
	}
	
	@PostMapping("/addStudent-classId={classId}")
	public ClassResponse AddStudentToClass(
			@PathVariable(value = "classId") Long classId,
			@Valid @RequestBody AddUserToClassRequest listUsers,
			@CurrentUser UserPrincipal currentUser) {
		System.out.printf("list ----- "+ listUsers.getListUser().size());
		return classService.addUserToClass(classId,  listUsers.getListUser(), currentUser);
	}
	
	@PostMapping("/deleteStudent-classId={classId}")
	public ClassResponse DeleteStudent(
			@PathVariable(value = "classId") Long classId,
			@Valid @RequestBody AddUserToClassRequest listUsers,
			@CurrentUser UserPrincipal currentUser) {
		System.out.printf("list ----- "+ listUsers.getListUser().size());
		return classService.deleteUsers(classId,  listUsers.getListUser(), currentUser);
	}
	
	@GetMapping("/allClasses")
    public PagedResponse<ClassResponse> getAllClassRoom(@CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return classService.getAllClasses(currentUser.getId(), page, size);
	}
	
	@PostMapping("/addLessonToClass")
	public ResponseEntity<?> AddLessonToClass(
			@RequestParam("classId") Long classId,
			@RequestParam("lessonId") Long lessonId,
			@CurrentUser UserPrincipal currentUser) {
		 classService.addLessonToClass(classId, lessonId, currentUser);
	return ResponseEntity.ok(new ApiResponse(true, "add lesson successfully"));
 
	}
	@PostMapping("/deleteLessonToClass")
	public ResponseEntity<?> deleteLessonFromClass(
			@RequestParam("classId") Long classId,
			@RequestParam("lessonId") Long lessonId,
			@CurrentUser UserPrincipal currentUser) {
		 classService.deleteLessonToClass(classId, lessonId, currentUser);
	return ResponseEntity.ok(new ApiResponse(true, "delete lesson successfully"));
 
	}
	

	@PostMapping("/sendJoinRequest")
	public ResponseEntity<?> sendJoinRequest(
			@RequestParam("classId") Long classId,
			@CurrentUser UserPrincipal currentUser) {
		 classService.sendJoinRequest(classId, currentUser);
	return ResponseEntity.ok(new ApiResponse(true, "your request is sent"));
 
	}
	
	@PostMapping("/acceptJoinRequest")
	public ResponseEntity<?> acceptJoinRequest(
			@RequestParam("requestId") Long requestId,
			@RequestParam("classId") Long classId,
			@RequestParam("userId") Long userId,
			@CurrentUser UserPrincipal currentUser) {
		 classService.acceptJoinRequest(requestId, classId, userId);
	return ResponseEntity.ok(new ApiResponse(true, "request is accepted"));
 
	}
	

	@PostMapping("/cancelJoinRequest")
	public ResponseEntity<?> cancelJoinRequest(
			@RequestParam("requestId") Long requestId) {
		 classService.cancelJoinRequest(requestId);
	return ResponseEntity.ok(new ApiResponse(true, "request is cancel"));
 
	}
	

	@PostMapping("/delete")
    @PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteClass(
			@RequestParam("classId") Long classId) {
		 classService.deleteClass(classId);
	return ResponseEntity.ok(new ApiResponse(true, "Class is deleted"));
 
	}
	
	@PostMapping("/edit")
	public ResponseEntity<?> editClass(
			@RequestParam("classId") Long classId,@RequestParam("classRoomName") String classRoomName ) {
		 classService.editClass(classId, classRoomName);
	return ResponseEntity.ok(new ApiResponse(true, "Class is updated"));
 
	}
}
